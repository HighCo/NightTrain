﻿using UnityEngine;
using System.Collections;

public class ViveController : MonoBehaviour
{
	public GameObject visual;
	public GameObject highlightWhenInteractable;

	enum Mode { None, Take, Move, Rotate };
	Mode mode;

	SteamVR_Controller.Device device;
	GameObject touchedObject;
	Rigidbody movingRigidbody;

	Moveable moveableObject;
	Vector3 offset;
	Quaternion rotationsOffset;
	bool moving;
	float angleOffet;

	new Rigidbody rigidbody;

	void Start ()
	{
		this.rigidbody = GetComponentInChildren<Rigidbody>();
		int index = (int)GetComponentInParent<SteamVR_TrackedObject>().index;
		this.device = SteamVR_Controller.Input(index);
		this.touchedObject = null;
		this.UpdateModel();
		this.moving = false;
	}

	void Update()
	{
		if(this.device == null) return;
		var controllerTransform = transform;

		if(this.device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
		{
			if(touchedObject != null)
			{
				if(this.touchedObject.GetComponent<Takeable>() != null)
				{
					this.mode = Mode.Take;
					this.movingRigidbody = this.touchedObject.GetComponent<Rigidbody>();
					this.offset = Quaternion.Inverse(this.movingRigidbody.transform.rotation) * (this.movingRigidbody.transform.position - controllerTransform.position);
					this.rotationsOffset = Quaternion.Inverse(controllerTransform.rotation) * this.movingRigidbody.transform.rotation;
					this.movingRigidbody.isKinematic = true;
					this.movingRigidbody.useGravity = false;
				}
				else
				if((this.moveableObject = touchedObject.GetComponentInParent<Moveable>()) != null)
				{
					this.movingRigidbody = this.moveableObject.GetComponent<Rigidbody>();
					this.offset = this.movingRigidbody.transform.localPosition - this.movingRigidbody.transform.parent.InverseTransformPoint(controllerTransform.position);

					if (this.moveableObject.type == Moveable.Type.Move)
					{
						this.mode = Mode.Move;
					}
					else
					if (this.moveableObject.type == Moveable.Type.Rotate)
					{
						this.mode = Mode.Rotate;
						Vector3 controllerPos = this.movingRigidbody.transform.localPosition - this.movingRigidbody.transform.parent.InverseTransformPoint(controllerTransform.position);
						var rotation = this.movingRigidbody.transform.localRotation.eulerAngles;
						this.angleOffet = GetAngleOfAxis(controllerPos, this.moveableObject.axis) - rotation.y;
					}
				}
			}
		}

		if(this.device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
		{
			if(this.mode == Mode.Take)
			{
				var rigidbody = this.movingRigidbody.GetComponent<Rigidbody>();
				rigidbody.isKinematic = false;
				rigidbody.useGravity = true;
			}
			this.mode = Mode.None;
		}
	}

	void FixedUpdate()
	{
		var controllerTransform = transform;

		if(this.mode == Mode.Move)
		{
			Vector3 controllerPos = this.movingRigidbody.transform.parent.InverseTransformPoint(controllerTransform.position);
			Vector3 pos = this.movingRigidbody.transform.localPosition;
			if(moveableObject.axis == Vector3.right)   pos.x = Extensions.Range(controllerPos.x + offset.x, moveableObject.min, moveableObject.max);
			if(moveableObject.axis == Vector3.up)      pos.y = Extensions.Range(controllerPos.y + offset.y, moveableObject.min, moveableObject.max);
			if(moveableObject.axis == Vector3.forward) pos.z = Extensions.Range(controllerPos.z + offset.z, moveableObject.min, moveableObject.max);
			var globalPos = this.movingRigidbody.transform.parent.TransformPoint(pos);
			this.movingRigidbody.MovePosition(globalPos);
		}
		else
		if(this.mode == Mode.Take)
		{ 
			this.movingRigidbody.transform.rotation = controllerTransform.rotation * this.rotationsOffset;
			this.movingRigidbody.transform.position = controllerTransform.position + (this.movingRigidbody.transform.rotation * this.offset);
		}
		else
		if(this.mode == Mode.Rotate)
		{
			Vector3 controllerPos = this.movingRigidbody.transform.localPosition - this.movingRigidbody.transform.parent.InverseTransformPoint(controllerTransform.position);
			//var angle = Mathf.Atan2(controllerPos.x, controllerPos.z)/Mathf.PI*180 - angleOffet;
			var controllerPositionAngle = GetAngleOfAxis(controllerPos, this.moveableObject.axis) - angleOffet + 360;
			while(controllerPositionAngle > moveableObject.min) controllerPositionAngle -= 360;

			var angle = Extensions.Range(controllerPositionAngle + 360, moveableObject.min, moveableObject.max);
			//var angle = controllerPositionAngle - angleOffet;

			var rotation = this.movingRigidbody.transform.rotation;
			this.movingRigidbody.transform.localRotation = Quaternion.Euler(rotation.x, angle, rotation.z);

			//Vector3 controllerPos = /*this.movingRigidbody.transform.localPosition -*/ this.movingRigidbody.transform.parent.InverseTransformPoint(controllerTransform.position);
			//float controllerPositionAngle = GetAngleOfAxis(controllerPos, this.moveableObject.axis);
			//float angle = Extensions.Range(controllerPositionAngle + angleOffet, moveableObject.min, moveableObject.max);
			//float angle = controllerPositionAngle + angleOffet;
			//SetAngleOfAxsis(this.movingRigidbody.transform, angle, this.moveableObject.axis);
		}

		visual.SetActive(this.mode != Mode.Take);
	}

	float GetAngleOfAxis(Vector3 pos, Vector3 axis)
	{
		float angle = 0;
		if(axis == Vector3.right)   angle = Mathf.Atan2(pos.y, pos.z);
		if(axis == Vector3.up)      angle = Mathf.Atan2(pos.x, pos.z);
		if(axis == Vector3.forward) angle = Mathf.Atan2(pos.x, pos.y);
		return angle / Mathf.PI * 180;
	}

	void SetAngleOfAxsis(Transform transform, float angle, Vector3 axis)
	{
		var rotation = transform.localRotation.eulerAngles;
		if(axis == Vector3.right)   transform.localRotation = Quaternion.Euler(angle, rotation.y, rotation.z);
		if(axis == Vector3.up)      transform.localRotation = Quaternion.Euler(rotation.x, angle, rotation.z);
		if(axis == Vector3.forward) transform.localRotation = Quaternion.Euler(rotation.x, rotation.y, angle);
	}

    void OnTriggerEnter(Collider other)
	{
		if(other.GetComponent<Interactable>() != null || other.GetComponent<Takeable>() != null) this.touchedObject = other.gameObject;
		this.UpdateModel();
    }

	void OnTriggerExit(Collider other)
	{
		if(this.touchedObject == other.gameObject) this.touchedObject = null;
		this.UpdateModel();
    }


	void UpdateModel()
	{
		this.highlightWhenInteractable.SetActive(this.touchedObject != null);
	}


}
