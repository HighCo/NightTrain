using System.Collections.Generic;
using UnityEngine;

public abstract class Logic
{
	Animation animation;

	public Logic()
	{
		this.animation = GameObject.FindObjectOfType<Animation>();
	}

	protected bool Get(string key) { return State.state.Contains(key); }
	protected void Set(string key) { State.state.Add(key); }
	protected void Unset(string key) { State.state.Remove(key); }
	protected void Trigger(string name) { this.animation.Play(name);  }
	protected bool Tapped(string key) { return State.taps.Contains(key);   }
	protected void PlaySound(string name)
	{
		AudioClip clip;
		if(!State.audioClips.TryGetValue(name, out clip))
			State.audioClips[name] = clip = Resources.Load<AudioClip>("Sounds/"+name);
		AudioSource.PlayClipAtPoint(clip, Vector3.zero);
	}

	public abstract void Update();
}

public class State
{
	public static HashSet<string> state = new HashSet<string>();
	public static HashSet<string> taps = new HashSet<string>();
	public static Dictionary<string, AudioClip> audioClips = new Dictionary<string, AudioClip>();
}
