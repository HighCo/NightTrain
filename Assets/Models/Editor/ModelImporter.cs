using UnityEditor;
using UnityEngine;

public class ModelImporter : AssetPostprocessor
{
	void OnPostprocessGameObjectWithUserProperties (GameObject go, string[] propNames, System.Object[] values)
	{	
		for(int i=0; i<propNames.Length; i++)
		{
			var key = propNames[i];
			var value = values[i];
			Debug.Log(go.name+": " +key+" = " +value);

			if(key == "invisible")
			{
				GameObject.DestroyImmediate(go.GetComponent<MeshRenderer>());
			}
			else
			if(key == "trigger")
			{
				var box = go.AddComponent<BoxCollider>();
				box.isTrigger = true;
				GameObject.DestroyImmediate(go.GetComponent<MeshRenderer>());
				go.AddComponent<AnimationTrigger>();
			}
			else
			if(key == "ground")
			{
				var meshCollider = go.AddComponent<MeshCollider>();
				GameObject.DestroyImmediate(go.GetComponent<MeshRenderer>());
			}
			else
			if(key == "moveable" || key == "rotatable")
			{
				var moveable = go.AddComponent<Moveable>();
				moveable.Parse(key, (string)value);
			}
			else
			if(key == "interactable")
			{
				go.AddComponent<Interactable>();
			}
			else
			if(key == "takeable")
			{
				var rigidbody = go.AddComponent<Rigidbody>();
				rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
				var collider = go.AddComponent<BoxCollider>();
				go.AddComponent<Takeable>();
			}
			else
			if(key == "collider")
			{
				var rigidbody = go.AddComponent<Rigidbody>();
				rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
				rigidbody.isKinematic = true;
				rigidbody.useGravity = false;
				var collider = go.AddComponent<MeshCollider>();
			}
		}
	}	
}
