using UnityEngine;

public class MouseInput : MonoBehaviour
{
	public float factor;

	new Camera camera;
	Vector3 lastPos;
	Vector3 cameraAngle;

	void Start ()
	{
		this.lastPos = Input.mousePosition;
		this.camera = Camera.main;
		this.cameraAngle = this.camera.transform.localEulerAngles;
	}
	
	void Update ()
	{
		var delta = Input.mousePosition - this.lastPos;
		this.cameraAngle.x += -delta.y*factor;
		this.cameraAngle.y += delta.x*factor;
		this.camera.transform.localEulerAngles = this.cameraAngle;
		this.lastPos = Input.mousePosition;
	}
}
