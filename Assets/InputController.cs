using System;
using UnityEngine;
using UnityEngine.VR;

public class InputController : MonoBehaviour
{
	public float speed = .1f;

	public Sprite defaultCursor;
	public Sprite interactableCursor;
	public SpriteRenderer cursorRenderer;

	public Transform cameraTransform;
	public Transform cursorTransform;
	public Transform playerTransform;

	Collider currentCollider;
	Logic logic;
	MeshCollider[] grounds;

	void Start()
	{
		this.currentCollider = null;
		this.logic = new MurderCabinLogic();
		this.grounds = FindObjectsOfType<MeshCollider>();
	}

	void Update ()
	{
		State.taps.Clear();
		var pos = cameraTransform.position;
		var delta = VRDevice.isPresent ? (Quaternion.Euler(0,180,0) * InputTracking.GetLocalRotation(VRNode.CenterEye) * Vector3.forward) : (cursorTransform.position - pos);

		Ray ray = new Ray(pos, delta);
		RaycastHit hitInfo;
		if(Physics.Raycast(ray, out hitInfo))
		{
			if(hitInfo.collider != this.currentCollider && hitInfo.collider.GetComponent<Interactable>() != null)
			{
				this.currentCollider = hitInfo.collider;
				this.UpdateCursor();
			}
		}
		else
		if(this.currentCollider != null)
		{
			this.currentCollider = null;
			this.UpdateCursor();
		}

		bool touch=false, touchDown=false;

		#if Oculus
				OVRInput.Update();
				touch = OVRInput.Get(OVRInput.Button.One);
				touchDown = OVRInput.GetDown(OVRInput.Button.One);
		#elif GoogleVR
				touch = GvrController.Touch;
				touchDown = GvrController.TouchDown;
		#elif GearVR
				touch = Input.GetButton("Tap");
				touchDown = Input.GetButtonDown("Tap");
		#endif

		if(this.currentCollider)
		{
			if (touchDown)
				State.taps.Add(this.currentCollider.name);
		}
		else
		if(touch)
			Move(delta);

		this.logic.Update();
	}

	void UpdateCursor()
	{
		this.cursorRenderer.sprite = this.currentCollider != null ? this.interactableCursor : this.defaultCursor;
	}

	void Move(Vector3 delta)
	{
		var start = playerTransform.position;
		var movement = new Vector3(delta.x, 0, delta.z).normalized * speed;

		if(IsOnGround(start + movement))
			playerTransform.position = start + movement;
		else
			playerTransform.position = start + FindPossibleMoveVector(start, movement);
	}

	Vector3 FindPossibleMoveVector(Vector3 start, Vector3 movement, int steps = 8)
	{
		Vector3 vector;
		float angle = Mathf.Atan2(movement.z, movement.x);
		float length = Mathf.Sqrt(movement.x*movement.x + movement.z*movement.z);
		float stepAngle = Mathf.PI / 2 / steps;
		for(float i = stepAngle; i < Mathf.PI / 2; i += stepAngle)
		{
			if(CheckAngle(start, angle+i, length, out vector))
				return vector;

			if(CheckAngle(start, angle-i, length, out vector))
				return vector;
		}
		return Vector2.zero;
	}

	bool CheckAngle(Vector3 start, float angle, float length, out Vector3 vector)
	{
		vector = new Vector3(Mathf.Cos(angle)*length, 0, Mathf.Sin(angle)*length);
		return IsOnGround(start + vector);
	}

	bool IsOnGround(Vector3 point)
	{
		Ray ray = new Ray(point, Vector3.down);
		RaycastHit hit;
		foreach(var ground in this.grounds)
			if(ground.Raycast(ray, out hit, 10))
				return true;
		return false;
	}

}
