using UnityEngine;
#if GoogleVR
public class DaydreamInput : MonoBehaviour
{
	public GameObject cursorObject;

	void Update ()
	{
		var orientation = GvrController.Orientation.eulerAngles;
		if(orientation.x > 180) orientation.x -= 360;
		if(orientation.y > 180) orientation.y -= 360;
		if(orientation.z > 180) orientation.z -= 360;

		this.cursorObject.transform.localPosition = new Vector3(
			MapRange(orientation.y, -45, 45,  -0.82f,  0.82f),
			MapRange(orientation.x, -45, 45,   0.46f, -0.46f),
			0.8f
		);
	}

	float MapRange(float value, float sourceStart, float sourceEnd, float targetStart, float targetEnd)
	{
		float factor = (value - sourceStart) / (sourceEnd - sourceStart);
		return targetStart + (targetEnd - targetStart) * factor;
	}
}
#endif