using System;

public class MurderCabinLogic : Logic
{
	public override void Update()
	{
		if(Tapped("BedHandle1") && !Get("bedUp"))
		{
			Trigger("Bed|RotationUp");
			PlaySound("squeak");
			Set("bedUp");
		}
		else
		if(Tapped("BedHandle1") && Get("bedUp"))
		{
			Trigger("Bed|RotationDown");
			PlaySound("squeak");
			Unset("bedUp");
		}
		else
		if(Tapped("BathroomDoorHandle1") && !Get("doorOpen"))
		{
			Trigger("BathroomDoor|Open");
			PlaySound("squeak");
			Set("doorOpen");

		}
		else
		if(Tapped("BathroomDoorHandle1") && Get("doorOpen"))
		{
			Trigger("BathroomDoor|Close");
			PlaySound("squeak");
			Unset("doorOpen");
		}
	}
}
