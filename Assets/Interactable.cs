using UnityEngine;

public class Interactable : MonoBehaviour
{
	void Start()
	{
		var collider = gameObject.AddComponent<BoxCollider>();
		collider.isTrigger = true;
	}
}
