﻿using UnityEngine;

public class Moveable : MonoBehaviour
{
	public enum Type { Move, Rotate };
	public Type type;

	public string properties;
	public Vector3 axis;
	public float min;
	public float max;

	public void Parse(string type, string properties)
	{
		this.properties = properties;
		this.type = type == "rotatable" ? Type.Rotate : Type.Move;

		if(!string.IsNullOrEmpty(properties))
		{
			foreach(var pair in this.properties.Split(','))
			{
				var item = pair.Split(':');
				var key = item[0].Trim();
				var value = item[1].Trim();
				if(key == "minY") { this.min = float.Parse(value); this.axis = new Vector3(0, 1, 0); } else
				if(key == "maxY") { this.max = float.Parse(value); this.axis = new Vector3(0, 1, 0); }
			}
			if(this.type == Type.Rotate)
			{
				var temp = -this.min;
				this.min = -this.max;
				this.max = temp;
			}
			var rigidBody = gameObject.AddComponent<Rigidbody>();
			rigidBody.isKinematic = true;
			rigidBody.useGravity = false;
			gameObject.AddComponent<MeshCollider>();
		}
	}
}
