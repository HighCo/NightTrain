bl_info = {
    "name": "Remove Parent Inverse",
    "category": "Object",
}
import bpy

class RemoveParentInverse(bpy.types.Operator):
    """Remove Parent Inverse"""
    bl_idname = "object.remove_parent_inverse"
    bl_label = "Remove Parent Inverse"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        ob = bpy.context.object

        # store a copy of the objects final transformation
        # so we can read from it later.
        ob_matrix_orig = ob.matrix_world.copy()

        # reset parent inverse matrix
        # (relationship created when parenting)
        ob.matrix_parent_inverse.identity()

        # re-apply the difference between parent/child
        # (this writes directly into the loc/scale/rot) via a matrix.
        ob.matrix_basis = ob.parent.matrix_world.inverted() * ob_matrix_orig
        return {'FINISHED'}

def register():
    bpy.utils.register_class(RemoveParentInverse)

def unregister():
    bpy.utils.unregister_class(RemoveParentInverse)

if __name__ == "__main__":
    register()